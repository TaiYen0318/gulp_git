var gulp = require('gulp');
var exec = require('child_process').exec;

gulp.task('add', function (cb) {
  exec('git add .', function (err, stdout, stderr) {
    cb(err);
  });
});

gulp.task('push', function (cb) {
  exec('git push', function (err, stdout, stderr) {
    cb(err);
  });
});

gulp.task('commit', function (cb) {
  exec('git commit -m "test"', function (err, stdout, stderr) {
    cb(err);
  });
});

gulp.task( 'default', gulp.series( 'add','commit', 'push'));
